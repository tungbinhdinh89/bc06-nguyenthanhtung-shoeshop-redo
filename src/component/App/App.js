// import logo from './logo.svg';
import Shoe_Shop from '../Shoe_Shop/Shoe_Shop';
import './App.css';

function App() {
  return (
    <div className="App">
      <Shoe_Shop />
    </div>
  );
}

export default App;
