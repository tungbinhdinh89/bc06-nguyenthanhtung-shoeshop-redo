import React, { Component } from 'react';
import Item_Shoe from './Item_Shoe';

export default class List_Shoe extends Component {
  renderListShoe = () => {
    return this.props.listShoe.map((item) => {
      return (
        <Item_Shoe
          dataShoe={item}
          handleChangeDetail={this.props.handleChangeDetail}
          handleAddItemToCart={this.props.handleAddItemToCart}
        />
      );
    });
  };
  render() {
    return <div>{this.renderListShoe()}</div>;
  }
}
