import React, { Component } from 'react';

export default class Cart_Shoe extends Component {
  renderCartShoe = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <img style={{ width: 80 }} src={item.image} />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangeNumberItem(item.id, -1);
              }}
              className="btn btn-danger">
              -
            </button>
            <strong className="mx-3">{item.number}</strong>
            <button
              onClick={() => {
                this.props.handleChangeNumberItem(item.id, 1);
              }}
              className="btn btn-primary">
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDeleteCartItem(item.id);
              }}
              className="btn border-danger text-danger">
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Price</th>
              <th>Image</th>
              <th>Quantity</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderCartShoe()}</tbody>
        </table>
      </div>
    );
  }
}
