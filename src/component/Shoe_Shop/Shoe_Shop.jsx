import React, { Component } from 'react';
import List_Shoe from './List_Shoe';
import dataShoe from './dataShoe';
import { shoeArr } from './dataShoe';
import Detail_Shoe from './Detail_Shoe';
import Cart_Shoe from './Cart_Shoe';

export default class Shoe_Shop extends Component {
  state = {
    shoeArr: shoeArr,
    detailShoe: shoeArr[0],
    cart: [],
  };

  // change detail information
  // dùng shoe thay cho id để lấy vị trí
  handleChangeDetail = (shoe) => {
    // console.log('shoe: ', shoe);
    this.setState({
      detailShoe: shoe,
    });
    // console.log('detailShoe before: ', this.state.shoeArr[0]);

    // console.log('detailShoe after: ', this.state.detailShoe);
    // console.log('shoe: ', shoe);
  };

  // add item to cart
  handleAddItemToCart = (shoe) => {
    // clone object let cloneObject = [...originObject]
    let cloneCart = [...this.state.cart];
    // th1 : sp chưa có trong giỏi hàng => tạo object mới gồn thông tin của object cũ, có thêm key soLuong:1 ( có push )
    // th2 : sp đã có trong giỏ hàng:update value của key soLuong ( không push )
    let index = cloneCart.findIndex((item) => {
      return item.id === shoe.id;
    });
    if (index === -1) {
      // create object from object with more property: let new object = {...object, newproperty: value}
      let newShoe = { ...shoe, number: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].number++;
    }

    this.setState({
      cart: cloneCart,
    });
  };

  // delere or remove item from cart
  handleDeleteCartItem = (id) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      console.log('cloneCart: ', cloneCart);
      console.log('id: ', id);

      return id === item.id;
    });
    cloneCart.splice(index, 1);
    console.log('index: ', index);
    this.setState({
      cart: cloneCart,
    });
  };

  // change number of item in cart
  handleChangeNumberItem = (id, option) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      //   console.log('index: ', index);
      return id === item.id;
    });
    cloneCart[index].number = cloneCart[index].number + option;
    cloneCart[index].number === 0 && cloneCart.splice(index, 1);

    this.setState({
      cart: cloneCart,
    });
  };

  render() {
    return (
      <div className="row">
        <div className="col-8">
          <Cart_Shoe
            cart={this.state.cart}
            handleDeleteCartItem={this.handleDeleteCartItem}
            handleChangeNumberItem={this.handleChangeNumberItem}
          />
        </div>
        <div className="col-4">
          <List_Shoe
            listShoe={this.state.shoeArr}
            handleChangeDetail={this.handleChangeDetail}
            handleAddItemToCart={this.handleAddItemToCart}
          />
        </div>
        <div className="">
          <Detail_Shoe detailShoe={this.state.detailShoe} />
        </div>
      </div>
    );
  }
}
