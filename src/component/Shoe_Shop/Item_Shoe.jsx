import React, { Component } from 'react';

export default class Item_Shoe extends Component {
  render() {
    return (
      <div className="col-6">
        <div className="card text-left">
          <img
            style={{ width: '10vw' }}
            className="card-img-top"
            src={this.props.dataShoe.image}
          />
          <div className="card-body text-center">
            <h4 className="card-title">{this.props.dataShoe.name}</h4>
            <h5 className="card-title">{this.props.dataShoe.price}</h5>
            <button
              className="btn btn-warning mr-4"
              onClick={() => {
                this.props.handleChangeDetail(this.props.dataShoe);
              }}>
              View Detail
            </button>
            <button
              className="btn btn-success"
              onClick={() => {
                this.props.handleAddItemToCart(this.props.dataShoe);
              }}>
              Add Cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}
