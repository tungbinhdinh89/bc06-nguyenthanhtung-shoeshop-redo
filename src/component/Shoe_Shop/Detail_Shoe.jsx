import React, { Component } from 'react';

export default class Detail_Shoe extends Component {
  render() {
    let {
      id,
      name,
      alias,
      price,
      description,
      shortDescription,
      quantity,
      image,
    } = this.props.detailShoe;
    return (
      <div>
        <div className="card">
          <img className="card-img-top" style={{ width: 200 }} src={image} />
          <div className="card-body">
            <h4 className="card-id">ID: {id}</h4>
            <h4 className="card-name">Model: {name}</h4>
            <h4 className="card-alias">Alias: {alias}</h4>
            <p className="card-price">Price: {price}$</p>
          </div>
          <ul className="list-group list-group-flush">
            <li className="list-quantity text-warning">Quantity: {quantity}</li>
            <li className="list-des">Description: {description}</li>
            <li className="list-review">Review : {shortDescription}</li>
          </ul>
        </div>
      </div>
    );
  }
}

//       {
//   "dataShoe": {
//     "id": 1,
//     "name": "Adidas Prophere",
//     "alias": "adidas-prophere",
//     "price": 350,
//     "description": "The adidas Primeknit upper wraps the foot with a supportive fit that enhances movement.\r\n\r\n",
//     "shortDescription": "The midsole contains 20% more Boost for an amplified Boost feeling.\r\n\r\n",
//     "quantity": 995,
//     "image": "http://svcy3.myclass.vn/images/adidas-prophere.png"
//   }
// }
